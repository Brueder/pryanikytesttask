﻿using PryanikyTestTask.Repositoryes.PostgreSQL;
using PryanikyTestTask.Services.Interfaces;
using PryanikyTestTask.Services;
using PryanikyTestTask.Entityes;

namespace PryanikyTestTask.Tests
{
    public class OrderTest
    {
        IProductService _productService;
        IOrderService _orderService;

        [SetUp]
        public void Setup()
        {
            TestHelper helper = new();

            _productService = new ProductService(
                helper.GetNullLogger<ProductService>(),
                new ProductRepository(helper.GetNullLogger<ProductRepository>(), helper.dBOptions));

            _orderService = new OrderService(
                helper.GetNullLogger<OrderService>(),
                new OrderRepository(helper.GetNullLogger<OrderRepository>(), helper.dBOptions));
        }

        [Test]
        public async Task GetProductIdsByOrderId()
        {
            Product product = new() { Name = "GetProductIdsByOrderId", Price = 200 };
            Order order = new() { Date = DateTime.Now };

            List<Product> resultProducts;

            int orderId = 0;
            int productId = 0;

            try
            {
                productId = await _productService.CreateAsync(product);
                order.ProductIds = new List<int> { productId };

                orderId = await _orderService.CreateAsync(order);
                resultProducts = (await _orderService.GetProductsByOrderId(orderId)).ToList();
            }
            finally
            {
                await _orderService.DeleteAsync(orderId);
                await _productService.DeleteAsync(productId);
            }

            Assert.That(order.ProductIds.Count() == resultProducts.Count);
            Assert.That(productId == resultProducts.First().ProductId);
        }

        [Test]
        public async Task CreateAndGet()
        {
            Product product = new() { Name = "CreateAndGet", Price = 200 };
            Order order = new() { Date = DateTime.Now };

            Order resultOrder;

            int orderId = 0;
            int productId = 0;

            try
            {
                productId = await _productService.CreateAsync(product);
                order.ProductIds = new List<int> { productId };

                orderId = await _orderService.CreateAsync(order);
                resultOrder = await _orderService.GetAsync(orderId);
            }
            finally
            {
                await _orderService.DeleteAsync(orderId);
                await _productService.DeleteAsync(productId);
            }

            Assert.That(resultOrder.Date.ToString("d") == order.Date.ToString("d") && resultOrder.ProductIds.Any(o => o == productId));
        }
    }
}
