﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using PryanikyTestTask.Entityes;

namespace PryanikyTestTask.Tests
{
    internal class TestHelper
    {
        private DBOptions _dBOptions = new();
        public DBOptions dBOptions { get { return _dBOptions; } }
        public TestHelper()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json");

            IConfigurationRoot configuration = builder.Build();
            _dBOptions.ConnectionString = configuration.GetConnectionString("DefaultConnection");
        }

        public ILogger<P> GetNullLogger<P>() => new Microsoft.Extensions.Logging.Abstractions.NullLogger<P>();
    }
}
