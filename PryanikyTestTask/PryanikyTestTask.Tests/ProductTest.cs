using PryanikyTestTask.Entityes;
using PryanikyTestTask.Repositoryes.PostgreSQL;
using PryanikyTestTask.Services;
using PryanikyTestTask.Services.Interfaces;

namespace PryanikyTestTask.Tests
{
    public class ProductTest
    {
        IProductService _productService;

        [SetUp]
        public void Setup()
        {
            TestHelper helper = new();

            _productService = new ProductService(
                helper.GetNullLogger<ProductService>(),
                new ProductRepository(helper.GetNullLogger<ProductRepository>(), helper.dBOptions));
        }

        [Test]
        [TestCase("Book", 300)]
        public async Task Create(string name, int price)
        {
            Product product = new() { Name = name, Price = price };
            int result = 0;

            try
            {
                result = await _productService.CreateAsync(product);
            }
            finally
            {
                await _productService.DeleteAsync(result);
            }


            Assert.That(result > 0);
        }

        [Test]
        [TestCase("Mouse", 300)]
        public async Task Get(string name, int price)
        {
            Product product = new() { Name = name, Price = price };
            int result = 0;
            Product resultObj;

            try
            {
                result = await _productService.CreateAsync(product);
                resultObj = await _productService.GetAsync(result);
            }
            finally
            {
                await _productService.DeleteAsync(result);
            }

            Assert.That(product.Price == resultObj.Price && product.Name == resultObj.Name);
        }

        [Test]
        [TestCase("Word", 300)]
        public async Task Update(string name, int price)
        {
            Product product = new() { Name = name, Price = price };
            Product testProduct = new() { Name = name + "TEST", Price = price };
            int result = 0;
            Product resultObj;

            try
            {
                result = await _productService.CreateAsync(product);
                await _productService.UpdateAsync(result, testProduct);
                resultObj = await _productService.GetAsync(result);
            }
            finally
            {
                await _productService.DeleteAsync(result);
            }

            Assert.That(testProduct.Name == resultObj.Name);
        }
    }
}