﻿using Serilog;

namespace PryanikyTestTask.API
{
    public static class LoggingExtension
    {
        public static IHostBuilder ConfigureApiSerilogLogging(this IHostBuilder builder)
        {
            return builder
                .ConfigureLogging((loggingBuilder) =>
                {
                    loggingBuilder.ClearProviders();
                })
                .UseSerilog((context, services, configuration) =>
                {
                    configuration
                    .MinimumLevel.Information()
                    .WriteTo.Console()
                    .WriteTo.File("Logs/logs.log", rollingInterval: RollingInterval.Day);

                });
        }
    }
}
