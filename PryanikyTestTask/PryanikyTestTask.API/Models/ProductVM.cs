﻿namespace PryanikyTestTask.API.Models
{
    public class ProductVM
    {
        public string Name { get; set; }
        public string? Description { get; set; }
        public int Price { get; set; }
    }
}
