﻿namespace PryanikyTestTask.API.Models
{
    public class OrderVM
    {
        public DateTime Date { get; set; }
        public IEnumerable<int> ProductIds { get; set; }
    }
}
