﻿using AutoMapper;
using PryanikyTestTask.Entityes;

namespace PryanikyTestTask.API.Models
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Order, OrderVM>();
            CreateMap<OrderVM, Order>();

            CreateMap<Product, ProductVM>();
            CreateMap<ProductVM, Product>();
        }
    }
}
