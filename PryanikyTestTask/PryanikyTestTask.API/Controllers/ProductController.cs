﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using PryanikyTestTask.API.Models;
using PryanikyTestTask.Entityes;
using PryanikyTestTask.Services.Interfaces;
using System.Data.SqlTypes;

namespace PryanikyTestTask.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ProductController : ControllerBase
    {
        private readonly ILogger<ProductController> _logger;
        private readonly IMapper _mapper;
        private readonly IProductService _productService;

        public ProductController(ILogger<ProductController> logger, IMapper mapper, IProductService productService)
        {
            _logger = logger;
            _mapper = mapper;
            _productService = productService;
        }

        [HttpGet("get")]
        public async Task<IActionResult> Get(int productId)
        {
            try
            {
                var value = await _productService.GetAsync(productId);
                return Ok(value);
            }
            catch (SqlNullValueException ex)
            {
                return BadRequest($"{nameof(SqlNullValueException)} {ex.Message}");
            }
        }

        [HttpGet("get_all")]
        public async Task<IActionResult> GetAll()
        {
            try
            {
                var value = await _productService.GetAllAsync();
                return Ok(value);
            }
            catch (SqlNullValueException ex)
            {
                return BadRequest($"{nameof(SqlNullValueException)} {ex.Message}");
            }
        }

        [HttpPost("create")]
        public async Task<IActionResult> Create(ProductVM product)
        {
            try
            {
                var value = await _productService.CreateAsync(_mapper.Map<Product>(product));
                return Ok(value);
            }
            catch (SqlNullValueException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost("update")]
        public async Task<IActionResult> Update([FromBody]Product product, [FromQuery]int productId)
        {
            try
            {
                var value = await _productService.UpdateAsync(productId, product);
                return Ok(value);
            }
            catch (SqlNullValueException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("delete")]
        public async Task<IActionResult> Delete(int productId)
        {
            await _productService.DeleteAsync(productId);
            return Ok();
        }
    }
}
