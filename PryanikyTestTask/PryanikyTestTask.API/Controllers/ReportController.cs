﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PryanikyTestTask.Entityes;

namespace PryanikyTestTask.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReportController : ControllerBase
    {
        private readonly IHttpClientFactory _httpClientFactory;

        public ReportController(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }

        [HttpGet("getreport")]
        public async Task<IActionResult> GetReport()
        {
            var client = _httpClientFactory.CreateClient();
            var result = await client.GetStringAsync("https://localhost:5001/Report/getreport");

            return Ok(result);
        }

        [HttpPost("setevent")]
        public async Task<IActionResult> SetEvent(Event ev)
        {
            var client = _httpClientFactory.CreateClient();
            var responce = await client.PostAsJsonAsync<Event>("https://localhost:5001/Report/setevent", ev);
            
            if(responce.StatusCode == System.Net.HttpStatusCode.OK)
            {
                return Ok();
            }

            return BadRequest();
        }
    }
}
