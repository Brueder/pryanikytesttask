﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PryanikyTestTask.API.Models;
using PryanikyTestTask.Entityes;
using PryanikyTestTask.Services;
using PryanikyTestTask.Services.Interfaces;
using System.Data.SqlTypes;

namespace PryanikyTestTask.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        private readonly ILogger<OrderController> _logger;
        private readonly IMapper _mapper;
        private readonly IOrderService _orderService;

        public OrderController(ILogger<OrderController> logger, IMapper mapper, IOrderService orderService)
        {
            _logger = logger;
            _mapper = mapper;
            _orderService = orderService;
        }

        [HttpGet("get")]
        public async Task<IActionResult> Get(int orderId)
        {
            try
            {
                var value = await _orderService.GetAsync(orderId);
                return Ok(value);
            }
            catch (SqlNullValueException ex)
            {
                return BadRequest($"{nameof(SqlNullValueException)} {ex.Message}");
            }
        }

        [HttpGet("getproductsbyorderId")]
        public async Task<IActionResult> GetProductsByOrderId(int orderId)
        {
            try
            {
                var value = await _orderService.GetProductsByOrderId(orderId);
                return Ok(value);
            }
            catch (SqlNullValueException ex)
            {
                return BadRequest($"{nameof(SqlNullValueException)} {ex.Message}");
            }
        }

        [HttpPost("create")]
        public async Task<IActionResult> Create(OrderVM order)
        {
            if(order.ProductIds.Count() == 0)
            {
                return BadRequest("ProductIds can not be null");
            }

            try
            {
                var value = await _orderService.CreateAsync(_mapper.Map<Order>(order));
                return Ok(value);
            }
            catch (SqlNullValueException ex)
            {
                return BadRequest($"{nameof(SqlNullValueException)} {ex.Message}");
            }
        }

        [HttpGet("delete")]
        public async Task<IActionResult> Delete(int orderId)
        {
            await _orderService.DeleteAsync(orderId);
            return Ok();
        }
    }
}
