﻿using Microsoft.Extensions.Logging;
using PryanikyTestTask.Entityes;
using PryanikyTestTask.Repositoryes.Interfaces;
using PryanikyTestTask.Services.Interfaces;
using System.Data.SqlTypes;

namespace PryanikyTestTask.Services
{
    public class ProductService : IProductService
    {
        private readonly ILogger<ProductService> _logger;
        private readonly IProductRepository _productRepository;

        public ProductService(ILogger<ProductService> logger, IProductRepository productRepository)
        {
            _logger = logger;
            _productRepository = productRepository;
        }

        public async Task<int> CreateAsync(Product product)
        {
            return await _productRepository.CreateAsync(product);  
        }

        public async Task DeleteAsync(int productId)
        {
            await  _productRepository.DeleteAsync(productId);
        }

        public async Task<IEnumerable<Product>> GetAllAsync()
        {
            var products = await _productRepository.GetAllAsync();
            if(products is null || products.Count() == 0)
            {
                throw new SqlNullValueException($"Method {nameof(GetAllAsync)} retun//ed null");
            }
            return products;
        }

        public async Task<Product> GetAsync(int productId)
        {
            var product = await _productRepository.GetAsync(productId);
            if (product is null)
            {
                throw new SqlNullValueException($"Method {nameof(GetAsync)} retuned null");
            }
            return product;
        }

        public async Task<int> UpdateAsync(int productId, Product product)
        {
            return await _productRepository.UpdateAsync(productId, product);
        }
    }
}
