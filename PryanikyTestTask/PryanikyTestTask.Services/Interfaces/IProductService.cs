﻿using PryanikyTestTask.Entityes;

namespace PryanikyTestTask.Services.Interfaces
{
    public interface IProductService
    {
        Task<Product> GetAsync(int productId);
        Task<IEnumerable<Product>> GetAllAsync();
        Task<int> CreateAsync(Product product);
        Task<int> UpdateAsync(int productId, Product product);
        Task DeleteAsync(int productId);
    }
}
