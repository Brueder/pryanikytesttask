﻿using PryanikyTestTask.Entityes;

namespace PryanikyTestTask.Services.Interfaces
{
    public interface IOrderService
    {
        Task<Order> GetAsync(int orderId);
        Task<IEnumerable<Product>> GetProductsByOrderId(int orderId);
        Task<int> CreateAsync(Order order);
        Task DeleteAsync(int orderId);
    }
}
