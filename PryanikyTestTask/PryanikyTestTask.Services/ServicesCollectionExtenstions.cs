﻿using Microsoft.Extensions.DependencyInjection;
using PryanikyTestTask.Services.Interfaces;

namespace PryanikyTestTask.Services
{
    public static class ServicesCollectionExtenstions
    {
        public static IServiceCollection ConfigureServices(this IServiceCollection services)
        {
            services.AddTransient<IOrderService, OrderService>();
            services.AddTransient<IProductService, ProductService>();

            return services;
        }
    }
}
