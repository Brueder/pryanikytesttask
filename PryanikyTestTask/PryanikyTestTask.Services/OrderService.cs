﻿using Microsoft.Extensions.Logging;
using PryanikyTestTask.Entityes;
using PryanikyTestTask.Repositoryes.Interfaces;
using PryanikyTestTask.Services.Interfaces;
using System.Data.SqlTypes;

namespace PryanikyTestTask.Services
{
    public class OrderService : IOrderService
    {
        private readonly ILogger<OrderService> _logger; 
        private readonly IOrderRepository _orderRepository;

        public OrderService(ILogger<OrderService> logger, IOrderRepository orderRepository)
        {
            _logger = logger;
            _orderRepository = orderRepository;
        }
        public async Task<int> CreateAsync(Order order)
        {
            int result;

            result = await _orderRepository.CreateAsync(order);
            await _orderRepository.InsertOrderProduct(result, order.ProductIds);

            return result;
        }

        public async Task DeleteAsync(int orderId)
        {
            await _orderRepository.DeleteAsync(orderId);
        }

        public async Task<Order> GetAsync(int orderId)
        {
            var order = await _orderRepository.GetAsync(orderId);
            order.ProductIds = await _orderRepository.GetProductIdsByOrderId(orderId);
            if (order is null)
            {
                throw new SqlNullValueException($"Method {nameof(GetAsync)} retuned null");
            }
            return order;
        }

        public async Task<IEnumerable<Product>> GetProductsByOrderId(int orderId)
        {
            var products = await _orderRepository.GetProductsByOrderId(orderId);
            if (products is null || products.Count() == 0)
            {
                throw new SqlNullValueException($"Method {nameof(GetProductsByOrderId)} retuned null");
            }
            return products;
        }
    }
}
