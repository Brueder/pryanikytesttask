﻿using Microsoft.AspNetCore.Mvc;
using PryanikyTestTask.Entityes;
using PryanikyTestTask.Report.Models;
using System.Data.SqlTypes;
using System.Text.Json;

namespace PryanikyTestTask.Report.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ReportController : ControllerBase
    {
        private readonly string _filePath = "report.json";
        private static UserReport CurrentReport;

        [HttpPost("setevent")]
        public async Task<IActionResult> SetEvent(Event ev)
        {
            if(ev.Value < 0 || string.IsNullOrEmpty(ev.Name))
            {
                return BadRequest("Event has false value");
            }

            IsReportNull(DateTime.UtcNow);

            CurrentReport.Value += ev.Value;
            await isTimeOver(CurrentReport);

            return Ok();
        }

        [HttpGet("getreport")]
        public async Task<IActionResult> GetReport()
        {
            IsReportNull(DateTime.UtcNow);
            await isTimeOver(CurrentReport); 

            return Ok(CurrentReport);
        }

        private async Task isTimeOver(UserReport currentReport)
        {
            var strDatetime = Request.HttpContext.Session.GetString("datetime");
            if (string.IsNullOrEmpty(strDatetime))
            {
                throw new ArgumentNullException();
            }

            DateTime sessionDateTime = JsonSerializer.Deserialize<DateTime>(strDatetime);
            var timeSpan = DateTime.UtcNow - sessionDateTime;
            var span = TimeSpan.FromMinutes(1);

            if(timeSpan > span)
            {
                var strDateTime = JsonSerializer.Serialize<DateTime>(DateTime.UtcNow);
                Request.HttpContext.Session.SetString("datetime", strDateTime);

                currentReport.DateTime = DateTime.Now;
                CurrentReport = currentReport;
            }
        }

        private void IsReportNull(DateTime dateTime)
        {
            if (CurrentReport is null)
            {
                CurrentReport = new UserReport() { DateTime = dateTime };
            }
        }
    }
}
