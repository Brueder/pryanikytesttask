﻿using Microsoft.AspNetCore.Builder;
using PryanikyTestTask.Report.Infrastructure;

namespace PryanikyTestTask.Report
{
    public class Program
    {
        internal static PryanikyTestTask.Report.Models.UserReport CurrentReport =  new(); //Вместо базы или файла
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            // Add services to the container.

            builder.Services.AddDistributedMemoryCache();
            builder.Services.AddSession();

            builder.Services.AddControllers();
            // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddSwaggerGen();

            builder.Services.AddCors(options => options.AddPolicy("CorsPolicy",
            builder =>
            {
                builder.AllowAnyOrigin();
                builder.AllowAnyHeader();
                builder.AllowAnyMethod();
            }
            ));

            var app = builder.Build();

            // Configure the HTTP request pipeline.
            if (app.Environment.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI();
            }
            app.UseSession();

            app.UseHttpsRedirection();

            app.UseAuthorization();

            app.MapControllers();

            app.UseSessinHandler();

            app.UseCors("CorsPolicy");

            app.Run();
        }
    }
}
