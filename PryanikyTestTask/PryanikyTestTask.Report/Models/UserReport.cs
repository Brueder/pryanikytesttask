﻿namespace PryanikyTestTask.Report.Models
{
    public class UserReport
    {
        public int Value { get; set; }
        public DateTime DateTime { get; set; }
    }
}
