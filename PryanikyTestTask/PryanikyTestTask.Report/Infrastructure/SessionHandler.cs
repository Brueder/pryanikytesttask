﻿using PryanikyTestTask.Report.Models;
using System.Text.Json;

namespace PryanikyTestTask.Report.Infrastructure
{
    public class SessionHandler
    {
        private readonly RequestDelegate _next;
        private readonly ILogger<RequestDelegate> _logger;
        private readonly string _filePath = "report.json";

        public SessionHandler(RequestDelegate next, ILogger<RequestDelegate> logger)
        {
            _next = next;
            _logger = logger;
        }

        public async Task Invoke(Microsoft.AspNetCore.Http.HttpContext content)
        {
            var currentDateTime = DateTime.UtcNow;

            if (!content.Session.Keys.Contains("datetime"))
            {
                var strDateTime = JsonSerializer.Serialize<DateTime>(currentDateTime);
                content.Session.SetString("datetime", strDateTime);
            }

            await _next.Invoke(content);
        }
    }
}
