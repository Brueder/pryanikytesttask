﻿namespace PryanikyTestTask.Report.Infrastructure
{
    public static class SessionHandlerMiddleware
    {
        public static IApplicationBuilder UseSessinHandler(this IApplicationBuilder app) => app.UseMiddleware<SessionHandler>();
    }
}
