﻿CREATE TABLE IF NOT EXISTS "Order"
(
    orderid serial UNIQUE NOT NULL PRIMARY KEY,
    date date NOT NULL
)