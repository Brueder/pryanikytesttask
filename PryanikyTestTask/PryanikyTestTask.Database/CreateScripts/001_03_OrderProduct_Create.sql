﻿CREATE TABLE IF NOT EXISTS "OrderProduct"
(
    orderid integer NOT NULL,
    productid integer NOT NULL,
    CONSTRAINT "OrderProduct_Order" FOREIGN KEY (orderid)
        REFERENCES public."Order" (orderid) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
        NOT VALID,
    CONSTRAINT "OrderProduct_Product" FOREIGN KEY (productid)
        REFERENCES public."Product" (productid) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
)
