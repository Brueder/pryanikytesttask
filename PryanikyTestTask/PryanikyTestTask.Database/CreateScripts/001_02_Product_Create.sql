﻿CREATE TABLE IF NOT EXISTS "Product"
(
    productid serial UNIQUE NOT NULL PRIMARY KEY,
    price int NOT NULL,
    name varchar(200) NOT NULL,
    description varchar(200) NULL
)
