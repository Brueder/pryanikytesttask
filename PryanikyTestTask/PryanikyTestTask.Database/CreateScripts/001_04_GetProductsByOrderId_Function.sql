﻿CREATE OR REPLACE FUNCTION public."GetProductsByOrderId"(
	order_id integer)
    RETURNS SETOF "Product" 
    LANGUAGE 'sql'
    COST 100
    VOLATILE PARALLEL UNSAFE
    ROWS 1000

AS $BODY$
	SELECT pr.productid, pr.price, pr.name, pr.description
		FROM public."OrderProduct" as op
		LEFT JOIN public."Product" as pr on op.productid = pr.productid
		WHERE op.orderid = order_id;
$BODY$;