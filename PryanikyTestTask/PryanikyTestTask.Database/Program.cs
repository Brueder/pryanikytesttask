﻿using DbUp;
using Microsoft.Extensions.Configuration;
using System.Reflection;

namespace PryanikyTestTask.Database
{
    class Program
    {
        public static int Main(string[] args)
        {

            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json");

            IConfigurationRoot configuration = builder.Build();
            var connectionString = configuration.GetConnectionString("DefaultConnection");

            EnsureDatabase.For.PostgresqlDatabase(connectionString);
            var dbUpgradeEngine = DeployChanges.To
                .PostgresqlDatabase(connectionString)
                .WithScriptsEmbeddedInAssembly(Assembly.GetExecutingAssembly())
                .WithVariablesDisabled()
                .LogToConsole()
                .Build();

            if (dbUpgradeEngine.IsUpgradeRequired())
            {
                Console.WriteLine("Upgrades have been detected. Upgrading database now...");
                var result = dbUpgradeEngine.PerformUpgrade();
                if (!result.Successful)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine(result.Error);
                    Console.ResetColor();
                    return -1;
                }
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine(value: "Upgrade completed successfully.");
                Console.ResetColor();
            }

            Console.WriteLine("Database is up to date now.");
            return 0;
        }
    }
}