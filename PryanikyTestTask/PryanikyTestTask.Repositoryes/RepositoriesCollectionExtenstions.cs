﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using PryanikyTestTask.Repositoryes.Interfaces;
using PryanikyTestTask.Repositoryes.PostgreSQL;

namespace PryanikyTestTask.Repositoryes
{
    public static class RepositoriesCollectionExtenstions
    {
        public static IServiceCollection ConfigureRepositories(this IServiceCollection services)
        {
            services.AddTransient<IOrderRepository, OrderRepository>();
            services.AddTransient<IProductRepository, ProductRepository>();
            
            return services;
        }
    }
}
