﻿using PryanikyTestTask.Entityes;

namespace PryanikyTestTask.Repositoryes.Interfaces
{
    public interface IOrderRepository
    {
        Task<Order> GetAsync(int orderId);
        Task InsertOrderProduct(int orderId, IEnumerable<int> productIds);
        Task<IEnumerable<Product>> GetProductsByOrderId(int orderId);
        Task<IEnumerable<int>> GetProductIdsByOrderId(int orderId);
        Task<int> CreateAsync(Order order);
        Task DeleteAsync(int orderId);
    }
}
