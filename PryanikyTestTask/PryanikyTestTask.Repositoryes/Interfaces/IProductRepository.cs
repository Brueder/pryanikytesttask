﻿using PryanikyTestTask.Entityes;

namespace PryanikyTestTask.Repositoryes.Interfaces
{
    public interface IProductRepository
    {
        Task<Product> GetAsync(int productId);
        Task<IEnumerable<Product>> GetAllAsync();
        Task<int> CreateAsync(Product product);
        Task<int> UpdateAsync(int productId, Product product);
        Task DeleteAsync(int productId);
    }
}
