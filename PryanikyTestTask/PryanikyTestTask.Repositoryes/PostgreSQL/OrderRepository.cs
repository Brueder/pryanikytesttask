﻿using Dapper;
using Microsoft.Extensions.Logging;
using Npgsql;
using PryanikyTestTask.Entityes;
using PryanikyTestTask.Repositoryes.Interfaces;
using System.Data;

namespace PryanikyTestTask.Repositoryes.PostgreSQL
{
    public class OrderRepository : IOrderRepository
    {
        private readonly ILogger<OrderRepository> _logger;
        private readonly DBOptions _options;

        public OrderRepository(ILogger<OrderRepository> logger, DBOptions options)
        {
            _logger = logger;
            _options = options;
        }


        public async Task<int> CreateAsync(Order order)
        {
            using (IDbConnection db = new NpgsqlConnection(_options.ConnectionString))
            {
                var query = "INSERT INTO public.\"Order\" (date) VALUES(@date) RETURNING orderid;";
                var result = await db.QueryAsync<decimal>(query, order);

                return (int)result.First();
            }
        }

        public async Task DeleteAsync(int orderId)
        {
            using (IDbConnection db = new NpgsqlConnection(_options.ConnectionString))
            {
                var query = "DELETE FROM public.\"Order\" WHERE orderid = @orderid";
                await db.ExecuteAsync(query, new { orderid = orderId });
            } 
        }

        public async Task<Order> GetAsync(int orderId)
        {
            using (IDbConnection db = new NpgsqlConnection(_options.ConnectionString))
            {
                var query = "SELECT * FROM public.\"Order\" WHERE orderid = @orderid;";
                var result = await db.QueryAsync<Order>(query, new { orderid = orderId });
                return result.FirstOrDefault();
            }
        }

        public async Task<IEnumerable<int>> GetProductIdsByOrderId(int orderId)
        {
            using (IDbConnection db = new NpgsqlConnection(_options.ConnectionString))
            {
                var query = "SELECT productid FROM public.\"OrderProduct\" WHERE orderid = @orderid";
                var result = await db.QueryAsync<int>(query, new { orderid = orderId });
                return result;
            }
        }

        public async Task<IEnumerable<Product>> GetProductsByOrderId(int orderId)
        {
            using (IDbConnection db = new NpgsqlConnection(_options.ConnectionString))
            {
                var query = "SELECT * FROM public.\"GetProductsByOrderId\"(@order_id)";
                var result = await db.QueryAsync<Product>(query, new {order_id = orderId });
                return result;
            }
        }

        public async Task InsertOrderProduct(int orderId, IEnumerable<int> productIds)
        {
            using (IDbConnection db = new NpgsqlConnection(_options.ConnectionString))
            {
                db.Open();
                var query = "INSERT INTO public.\"OrderProduct\"(orderid, productid) VALUES (@orderid, @productid);";

                using (var transaction = db.BeginTransaction())
                {
                    foreach(var id in productIds)
                    {
                        await db.ExecuteAsync(query, new { orderid = orderId, productid = id });
                    }

                    transaction.Commit();
                }
            }
        }
    }
}
