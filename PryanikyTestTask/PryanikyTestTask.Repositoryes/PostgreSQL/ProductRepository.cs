﻿using Microsoft.Extensions.Logging;
using PryanikyTestTask.Entityes;
using PryanikyTestTask.Repositoryes.Interfaces;
using Dapper;
using System.Data;
using Npgsql;

namespace PryanikyTestTask.Repositoryes.PostgreSQL
{
    public class ProductRepository : IProductRepository
    {
        private readonly ILogger<ProductRepository> _logger;
        private readonly DBOptions _options;

        public ProductRepository(ILogger<ProductRepository> logger, DBOptions options)
        {
            _logger = logger;
            _options = options;
        }

        public async Task<int> CreateAsync(Product product)
        {
            using (IDbConnection db = new NpgsqlConnection(_options.ConnectionString))
            {
                var query = "INSERT INTO public.\"Product\"(price, name, description) VALUES(@price, @name, @description) RETURNING productid;";
                var result = await db.QueryAsync<decimal>(query, product);

                if (result == null) return -1;
                {
                    return (int)result.First();
                }
            }
        }

        public async Task DeleteAsync(int productId)
        {
            using (IDbConnection db = new NpgsqlConnection(_options.ConnectionString))
            {
                var query = "DELETE FROM public.\"Product\" WHERE productid = @productid";
                await db.ExecuteAsync(query, new { productid = productId });
            }
        }

        public async Task<IEnumerable<Product>> GetAllAsync()
        {
            using (IDbConnection db = new NpgsqlConnection(_options.ConnectionString))
            {
                var query = "SELECT * FROM public.\"Product\"";
                var result = await db.QueryAsync<Product>(query);

                return result;
            }
        }

        public async Task<Product> GetAsync(int productId)
        {
            using (IDbConnection db = new NpgsqlConnection(_options.ConnectionString))
            {
                var query = "SELECT * FROM public.\"Product\" WHERE productid = @productid;";
                var result = await db.QueryAsync<Product>(query, new { productid = productId });
                return result.FirstOrDefault();
            }
        }

        public async Task<int> UpdateAsync(int productId, Product product)
        {
            using (IDbConnection db = new NpgsqlConnection(_options.ConnectionString))
            {
                product.ProductId = productId;
                var query = "UPDATE public.\"Product\" SET price=@price, name=@name, description=@description WHERE productid = @productid RETURNING productid";
                var result = await db.QueryAsync<int>(query, product);
                return result.FirstOrDefault();
            }
        }
    }
}
