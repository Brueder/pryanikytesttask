﻿namespace PryanikyTestTask.Entityes
{
    public class Event
    {
        public string Name { get; set; }
        public int Value { get; set; }
    }
}
