﻿namespace PryanikyTestTask.Entityes
{
    public class Order
    {
        public int OrderId { get; set; }
        public DateTime Date { get; set; }
        public IEnumerable<int> ProductIds { get; set; }
    }
}
