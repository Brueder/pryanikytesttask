# Пряники. Тестовое задание

Далее попробую описать.

Выполненны оба задание. Второе я мог неправельно понять, дальше опишу.

Все решение состоит из нескольких проектов:
- Api. Главное апи приложения
- Entityes. Общие сущности
- Reposytoryes. Слой репозитория
- Services. Слой сервисов 
- Tests. Интеграционные тесты на NUnit
- Database. Небольшой сервис для выполнения sql команд
- Report. Микросервис второго задания

Собрать проект можно с помощью:
```bash
docker-compose up -d
```

Или запустите одновременно проекты: Api, Report, Database, незабыв изменить appsetting.json в Api и Database

## База 
В качестве базы использовался postgre. В docker-compose она собирается автоматически.

### Сборка

Чтобы создать схему нужно запустить проект Database. Он берёт sql запросы из папки и выполняет их, создав базу

Путь к базе находиться в appsetting.json

```json
{
  "ConnectionStrings": {
    "DefaultConnection": "Server=192.168.1.74; Port=5432;User Id=postgres;Password=password;Database=PryanikyTestTask"
  }
}
```

### Table
Нужен чтобы сохранить информацию о заказе. Тут по минимому. 

Имеет такую схему:

```sql
orderid serial UNIQUE NOT NULL PRIMARY KEY,
date date NOT NULL
```

### Product
Нужен для сохранения информации о продукте. Я не стал делать дополнительные таблицы, по типу категории

Имеет такую схему:

```sql
productid serial UNIQUE NOT NULL PRIMARY KEY,
price int NOT NULL,
name varchar(200) NOT NULL,
description varchar(200) NULL
```

### OrderProduct
Связь многие ко многим для продукта и заказа

Имеет такую схему:

```sql
orderid integer NOT NULL,
productid integer NOT NULL,
CONSTRAINT "OrderProduct_Order" FOREIGN KEY (orderid)
    REFERENCES public."Order" (orderid) MATCH SIMPLE
    ON UPDATE CASCADE
    ON DELETE CASCADE
    NOT VALID,
CONSTRAINT "OrderProduct_Product" FOREIGN KEY (productid)
    REFERENCES public."Product" (productid) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION
    NOT VALID
```

### GetProductByOrderId
Функция, которая вытаскивает из базы все продукты по orderid 

Имеет такую схему:

```sql 
CREATE OR REPLACE FUNCTION public."GetProductsByOrderId"(
	order_id integer)
    RETURNS SETOF "Product" 
    LANGUAGE 'sql'
    COST 100
    VOLATILE PARALLEL UNSAFE
    ROWS 1000

AS $BODY$
	SELECT pr.productid, pr.price, pr.name, pr.description
		FROM public."OrderProduct" as op
		LEFT JOIN public."Product" as pr on op.productid = pr.productid
		WHERE op.orderid = order_id;
$BODY$;
```

## Reposyrty и Service
Проект построе на трехслойке. В качестве orm, я использовал Dapper

Каждый из этих слоёв имет интерфейсы и методы расширения для них.

Описывать тут нечего. Обычный код

## Api

Главный вход в приложение. Имеет 2 контроллера по заданию (Order, product) для изменения\чтения каждой сущности

Из интересного, тут используется AutoMapper, чтобы при добавлении объекта сущности не пришлось писать id, так как это не нужно

ReportController перенаправляет запрос в проект Report.

Путь к базе находиться в appsetting.json

Все точки входа:

![enter](./.README_Images/enter.png)


```json
{
  "ConnectionStrings": {
    "DefaultConnection": "Server=192.168.1.74; Port=5432;User Id=postgres;Password=password;Database=PryanikyTestTask"
  }
}
```

## Report

Сервис для второго задания. Написанно в спешке под конец, поэтому кодом не доволен. Вместо базы тут статическое поле. Я понял это задание как некий сервис для сохранения отчета.

Когда пользователь выполняет запрос, middlleware записывает текущее время в Session. Это нужно чтобы не делать лишние запросы в базу, если бы база была.

Имеет 2 метода в контроллере:
- SetEvent

    Добавляет value и записывает последнее время

- GetReport

    Возвращает данные в виде: текущее время c интервалами в 1 минуту

## Тесты

Тесты тут используют базу, поэтому измените путь к базе в appsetting.json

```json
{
  "ConnectionStrings": {
    "DefaultConnection": "Server=192.168.1.74; Port=5432;User Id=postgres;Password=password;Database=PryanikyTestTask"
  }
}
```

Тестами не очень доволен. Писал спешке


*Я надеюсь, что кто-то дочитал до конца)*